package bio.one;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * 客户端
 */
public class Client {
    public static void main(String[] args) {
        try {
            // 通过socket请求服务端的链接
            Socket socket = new Socket("127.0.0.1", 9999);
            OutputStream stream = socket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(stream);

            printWriter.println("服务端你好");
            printWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
