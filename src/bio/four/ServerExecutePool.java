package bio.four;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ServerExecutePool {
    private ExecutorService executorService;


    public ServerExecutePool(Integer coreNum, Integer maxSize, Integer queueNum){
        this.executorService = new ThreadPoolExecutor(coreNum,maxSize,120, TimeUnit.SECONDS,new LinkedBlockingDeque<>(queueNum));
    }


    public void execute(Runnable target){
        executorService.execute(target);
    }
}
