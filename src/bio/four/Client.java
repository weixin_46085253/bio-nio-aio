package bio.four;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * 客户端
 */
public class Client {
    public static void main(String[] args) {
        try {
            // 通过socket请求服务端的链接
            Socket socket = new Socket("127.0.0.1", 9999);
            OutputStream stream = socket.getOutputStream();
            PrintWriter printWriter = new PrintWriter(stream);

            Scanner scanner = new Scanner(System.in);
            while (true){
                System.out.println("请输入：");
                String nextLine = scanner.nextLine();
                printWriter.println(nextLine);
                printWriter.flush();
            }


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
