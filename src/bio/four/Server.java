package bio.four;

import bio.three.ServerSocketThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 服务端
 */
public class Server {
    public static void main(String[] args) {
        // [1] 创建一个 serverSocket
        try {
            System.out.println("------服务端开启-------");
            ServerSocket serverSocket = new ServerSocket(9999);
            // [2] 获取  socket
            int i = 1;
            ServerExecutePool executePool = new ServerExecutePool(3, 6, 10);
            while (true){
                Socket socket = serverSocket.accept();
                System.out.println("接收到第"+i+"个客户端的链接");
                String name = "客户端"+i;
                ServerRunnable runnable = new ServerRunnable(socket);
                executePool.execute(runnable);
                i++;
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
//            throw new RuntimeException(e);
        }
    }
}
