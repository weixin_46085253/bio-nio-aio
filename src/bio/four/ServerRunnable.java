package bio.four;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerRunnable implements Runnable{

    private Socket socket;

    public ServerRunnable(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            while ((msg = reader.readLine()) != null){
                System.out.println("接收到来自客户端的消息："+msg);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
