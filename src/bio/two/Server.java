package bio.two;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 服务端
 */
public class Server {
    public static void main(String[] args) {
        // [1] 创建一个 serverSocket
        try {
            System.out.println("------服务端开启-------");
            ServerSocket serverSocket = new ServerSocket(9999);
            // [2] 获取  socket
            Socket socket = serverSocket.accept();

            // [3] 获取输入流
            InputStream inputStream = socket.getInputStream();

            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            while ((msg = bf.readLine()) != null){
                System.out.println("接收到来自客户端："+msg);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
