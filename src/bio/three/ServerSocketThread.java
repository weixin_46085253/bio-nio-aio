package bio.three;

import java.io.*;
import java.net.Socket;

public class ServerSocketThread extends Thread{

    private Socket socket;
    private String name;


    public ServerSocketThread(Socket socket, String name){
        this.socket = socket;
        this.name = name;
    }


    @Override
    public void run() {

        try {
            InputStream inputStream = socket.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
            String msg;
            while ((msg = bf.readLine()) != null){
                System.out.println("接收到来自"+name+"的消息:"+msg);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        super.run();
    }
}
